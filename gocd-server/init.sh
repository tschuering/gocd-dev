#!/bin/bash

PASSWD="/etc/go/passwd"

if [ "$1" == "--init" ] ; then
  if [ -f "$PASSWD" ] ; then
    rm $PASSWD
    echo "$PASSWD removed"
  fi
fi

if [ "$GO_USERS" != "" ] ; then
  if [ ! -f "$PASSWD" ] ; then
    FLAGS=-sbc
    PAIRS=(${GO_USERS//\;/ })
    for i in "${!PAIRS[@]}" ; do
        PAIR=(${PAIRS[i]//\:/ })
        USR="${PAIR[0]}"
        PWD="${PAIR[1]}"
        htpasswd $FLAGS $PASSWD $USR $PWD
        FLAGS=-sb
    done

    echo "$PASSWD created"
  else
    echo "$PASSWD exists"
  fi
else
  echo "No file '$PASSWD'"
fi
